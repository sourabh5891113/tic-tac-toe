import React, { useState } from 'react'
import Board from './Board';

export default function Game() {
    const [history, setHistory] = useState([
      { squares: Array(9).fill(null), moveLocation: null },
    ]);
    const [styles, setStyles] = useState(
      Array(9).fill({ backgroundColor: "white" })
    );
    const [currentMove, setCurrentMove] = useState(0);
    const [order, setOrder] = useState(true);
    const xIsNext = currentMove % 2 === 0;
    const currentSquares = history[currentMove].squares;
  
    function handlePlay(nextSquares, moveIndex) {
      const row = Math.floor(moveIndex / 3);
      const col = moveIndex % 3;
      const moveLocation = `(${row + 1}, ${col + 1})`;
      const nextHistory = history
        .slice(0, currentMove + 1)
        .concat([{ squares: nextSquares, moveLocation }]);
      setHistory(nextHistory);
      setCurrentMove(nextHistory.length - 1);
    }
    function jumpTo(nextMove) {
      setCurrentMove(nextMove);
    }
    const moves = history.map((step, move) => {
      let description;
      let len = history.length - 1;
      let newMove = order ? move : len - move;
      let moveLocation = history[newMove].moveLocation;
      if (move === len) return null;
      description =
        move > 0
          ? `Go to move #${newMove} (${moveLocation})`
          : "Go to start game";
      return (
        <li key={move}>
          <button onClick={() => jumpTo(newMove)}>{description}</button>
        </li>
      );
    });
    function handleSort() {
      setOrder(!order);
    }
    return (
      <div className="game">
        <div className="game-board">
          <Board
            xIsNext={xIsNext}
            squares={currentSquares}
            onPlay={handlePlay}
            styles={styles}
            setStyles= {setStyles}
          />
        </div>
        <div style={{ marginLeft: "30px" }}>
          <p>`You are at move #{currentMove}`</p>
          <button onClick={handleSort}>
            sort moves by {order ? "descending" : "ascending"} order
          </button>
        </div>
        <div className="game-info">
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }