import React, { useEffect } from 'react'
import Square from './Square';

export default function Board({ xIsNext, squares, onPlay,styles, setStyles }) {
    const winner = calculateWinner(squares);
    let status;
    useEffect(() => {
      if (winner) {
          const newStyles = Array(9).fill({backgroundColor: "white" });
          winner.boxes.forEach((index) => {
              newStyles[index] = { backgroundColor: "lightgreen" };
          });
          //only update when newStyles is different
          if(JSON.stringify(styles) !== JSON.stringify(newStyles)){
            setStyles(newStyles);
          }
      }else{
        const defaltStyles = Array(9).fill({backgroundColor: "white" });
        if(JSON.stringify(styles) !== JSON.stringify(defaltStyles)){
          setStyles(defaltStyles);
        }
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [winner]);
    const isEnd = (squares) => {
      return squares.every((square) => square !== null);
    };
    if (winner) {
      status = "Winner: " + winner.winner;
    } else if (isEnd(squares)) {
      status = "Match Draw";
    } else {
      status = "Next player: " + (xIsNext ? "X" : "O");
    }
  
    function handleClick(i) {
      if (calculateWinner(squares) || squares[i]) {
        return;
      }
      const nextSquares = squares.slice();
      nextSquares[i] = xIsNext ? "X" : "O";
      onPlay(nextSquares, i);
    }
    function createBoard() {
      const renderSquare = (i) => (
        <Square
          key={i}
          style={styles[i]}
          value={squares[i]}
          onSquareClick={() => handleClick(i)}
        />
      );
      const boardRows = [];
      for (let row = 0; row < 3; row++) {
        let squares = [];
        for (let col = 0; col < 3; col++) {
          squares.push(renderSquare(3 * row + col));
        }
        boardRows.push(
          <div key={row} className="board-row">
            {squares}
          </div>
        );
      }
      return boardRows;
    }
    return (
      <>
        <div className="status">{status}</div>
        {createBoard()}
      </>
    );
  }


  function calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        return { winner: squares[a], boxes: lines[i] };
      }
    }
    return null;
  }