import React from "react";

export default function Square({ value, style, onSquareClick }) {
  return (
    <button className="square" style={style} onClick={onSquareClick}>
      {value}
    </button>
  );
}
